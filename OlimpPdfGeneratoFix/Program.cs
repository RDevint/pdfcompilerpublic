﻿using PdfSharp.Drawing;
using PdfSharp.Pdf;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;

namespace OlimpPdfGenerator
{
    class Program
    {
        static string workFolder = Directory.GetCurrentDirectory();
        static string tempFolder = null;
        static void Main(string[] args)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            PdfDocument document = new PdfDocument();
            //
            int pages = 0;
            foreach (var a in args)
            {
                if (File.Exists(a))
                {
                    Console.WriteLine("Processing " + a);
                    if (TryAddPage(document, a))
                    {
                        pages++;
                    }
                }
            }
            if (pages > 0)
            {
                document.Save(workFolder + "\\test.pdf");
            }
            Console.WriteLine("Complete " + pages + " pages");
            Console.ReadLine();
            FlushTempFolder();
        }

        //Add page to existing PDF doc
        static bool TryAddPage(PdfDocument document, string path)
        {
            var image = LoadXImage(path);
            if (image != null)
            {
                var page = document.AddPage();
                page.Size = PdfSharp.PageSize.A4;
                if (ImageIsLandscape(image))
                {
                    var w = page.Width;
                    var h = page.Height;
                    page.Height = w;
                    page.Width = h;
                }
                XGraphics gfx = XGraphics.FromPdfPage(page);
                gfx.DrawImage(image, new XPoint(0, 0));
                return true;

            }
            return false;

        }

        static XImage LoadXImage(string path)
        {
            var imagePath = AssignImage(path);
            if (imagePath != null)
            {
                XImage image = XImage.FromFile(imagePath);
                return image;
            }
            return null;
        }

        static bool ImageIsLandscape(XImage img)
        {
            if (img.PointWidth > img.PointHeight)
            {
                return true;
            }
            return false;
        }

        static string AssignImage(string path)
        {

            if (!File.Exists(path))
            {
                return null;
            }
            var imageFileInfo = new FileInfo(path);
            if (imageFileInfo.Extension.ToLower() == ".bmp")
            {
                var bitmap = new Bitmap(path);
                string name = imageFileInfo.Name.Split('.')[0];
                return ConvertBMP_JPG(bitmap, name);
            }
            if (imageFileInfo.Extension.ToLower() == ".jpg")
            {
                return path;
            }
            return null;
        }


        static string ConvertBMP_JPG(Bitmap bitmap, string name)
        {
            RequireTempFolder();
            EncoderParameters encoderParameters = new EncoderParameters(1);
            encoderParameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 65L);
            bitmap.Save(tempFolder + "\\" + name + ".jpg", GetEncoder(ImageFormat.Jpeg), encoderParameters);
            return tempFolder + "\\" + name + ".jpg";

        }

        static void RequireTempFolder()
        {
            if (tempFolder == null)
            {
                tempFolder = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                Directory.CreateDirectory(tempFolder);
            }

        }

        public static void FlushTempFolder()
        {
            if (tempFolder != null && Directory.Exists(tempFolder))
            {
                System.IO.DirectoryInfo di = new DirectoryInfo(tempFolder);

                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
                foreach (DirectoryInfo dir in di.GetDirectories())
                {
                    dir.Delete(true);
                }

                di.Delete();
            }

        }

        public static ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();

            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }

            return null;
        }


    }
}
