﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentGenerator
{
    //Класс приведения имени pdf к требуемому формату
    public class PdfRenamingCompiler : IPdfCompiler
    {
        string source = null;
        public PdfRenamingCompiler(string source)
        {
            this.source = source;
        }
        public bool CopyTo(string path)
        {
            try
            {
                File.Copy(source, path, true);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public string PreviewDocument()
        {
            return source;
        }

        public bool RemoveSources()
        {
            try
            {

                File.Delete(source);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public string GetSourcesDirectory()
        {

            var info = new FileInfo(source);
            return info.DirectoryName;


        }
    }
}
