﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DocumentGenerator
{
    //Класс выбора компилятора (pdf, картинки или папка с картинками)
    public class CompilerSwitch
    {
        public static IPdfCompiler AssignCompilerTo(string[] paths)
        {
            if (isImages(paths))
            {
                return new PdfFromImageCompiler(paths);
            }
            if (isSinglePdf(paths))
            {
                return new PdfRenamingCompiler(paths[0]);
            }
            if (isDirectory(paths))
            {
                processDirectory(paths);                
            }
            return null;
        }

        //Является ли обрабатываемый файл картинкой
        private static bool isImages(string[] paths)
        {
            var r_bmp = new Regex(@".*(jpg|bmp)");
            if (r_bmp.IsMatch(paths[0].ToLower()))
            {
                return true;
            }
            return false;

        }
        //...или pdf
        private static bool isSinglePdf(string[] paths)
        {
            var r_bmp = new Regex(@".*(pdf)");
            if (r_bmp.IsMatch(paths[0].ToLower()))
            {
                return true;
            }
            return false;

        }
        //... или папкой с картинками
        private static bool isDirectory(string[] paths)
        {
            return Directory.Exists(paths[0]);
        }

        
        private static void processDirectory(string[] paths)
        {
            
            foreach(string dir in paths)
            {
                if (Directory.Exists(dir))
                {
                    //1. Проверка на правильность имени директории
                    var DirNameRgr = new Regex(@"\d\d\.\d\d\.\d\d(-\d\d\.\d\d\.\d\d){0,1} - .*");
                    if (!DirNameRgr.IsMatch(dir))
                    {
                        System.Windows.Forms.MessageBox.Show("Директория " + dir + " имеет неверный формат наименования, пропуск");
                        continue;
                    }
                    string filename = getCompiledFileName(dir);
                    //1_. Проверка расположения
                    if (filename == null)
                    {
                        System.Windows.Forms.MessageBox.Show("Директория " + dir + " расположена в неподдерживаемом каталоге, пропуск");
                        continue;
                    }

                    //2. Проверка на уже существующий файл 
                    if (File.Exists(dir+"\\"+filename))
                    {
                        continue;
                    }
                    //3. Проверка на наличие pdf файла
                    var files = Directory.GetFiles(dir, "*.pdf", SearchOption.TopDirectoryOnly);
                    if (files.Length == 1)
                    {
                        var compiler = new PdfRenamingCompiler(files[0]);
                        compiler.CopyTo(dir + "\\" + filename);
                        if (File.Exists(dir + "\\" + filename))
                        {
                            compiler.RemoveSources();
                        }
                        continue;
                    }

                    //4. Наличие нескольких pdf 
                    ///TODO: Когда-нибудь...

                    //5. Если несколько изображений 
                    files = Directory.GetFiles(dir, "*.BMP",SearchOption.TopDirectoryOnly);
                    if (files.Length > 0)
                    {
                        var compiler = new PdfFromImageCompiler(files);
                        compiler.CopyTo(dir + "\\" + filename);
                        if (File.Exists(dir + "\\" + filename))
                        {
                            compiler.RemoveSources();
                        }
                        continue;
                    }
                    files = Directory.GetFiles(dir, "*.jpg", SearchOption.TopDirectoryOnly);
                    if (files.Length > 0)
                    {
                        var compiler = new PdfFromImageCompiler(files);
                        compiler.CopyTo(dir + "\\" + filename);
                        if (File.Exists(dir + "\\" + filename))
                        {
                            compiler.RemoveSources();
                        }
                        continue;
                    }
                }
            }
        }

        private static string getCompiledFileName(string dir)
        {
            if (dir.Contains("! Регламенты мероприятий"))
            {
                return "Регламент.pdf";
            }
            if (dir.Contains("! Отчёты мероприятий"))
            {
                return "Отчёт.pdf";
            }
            return null;
        }
   
    }
}
