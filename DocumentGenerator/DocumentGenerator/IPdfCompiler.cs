﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentGenerator
{
    //Интерфейс сборщика pdf файла
    public interface IPdfCompiler
    {
        bool CopyTo(string path);
        string PreviewDocument();
        bool RemoveSources();
        string GetSourcesDirectory();


    }
}
