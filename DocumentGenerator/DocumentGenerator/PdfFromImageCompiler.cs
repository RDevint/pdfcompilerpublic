﻿using PdfSharp.Drawing;
using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentGenerator
{
    //Класс сборщика pdf из картинок
    public class PdfFromImageCompiler:IPdfCompiler
    {
        string tempFolder = null;
        List<string> sources = new List<string>();
        PdfDocument document = new PdfDocument();
        int pages = 0;
        public PdfFromImageCompiler(string[] paths)
        {
            //Сортировка по имени файлов   
            Array.Sort(paths, StringComparer.InvariantCulture);
            foreach (var path in paths)
            {
                if (File.Exists(path))
                {
                    Console.WriteLine("Processing " + path);
                    if (TryAddPage(document, path))
                    {
                        pages++;
                        sources.Add(path);
                    }
                }
            }
            if (pages > 0)
            {
                //Сохранение документа для предпросмотра во временную папку
                document.Save(tempFolder + "\\compiled.pdf");
            }
        }

        ~PdfFromImageCompiler()
        {
            document.Close();
            FlushTempFolder();
        }


        public bool CopyTo(string path)
        {
            try
            {
                document.Save(path);
                //File.Copy(tempFolder + "\\compiled.pdf", path, true);
            }catch(Exception)
            {
                return false;
            }
            
            return true;
        }

        public bool RemoveSources()
        {
            document.Close();
            try
            {
                foreach(string s in sources)
                {
                    File.Delete(s);
                }
                return true;
            }
            catch(Exception e)
            {
                
                return false;
            }
        }
        public string PreviewDocument()
        {
            if (pages > 0)
            {
                return tempFolder + "\\compiled.pdf";
            }
            return null;
        }

        public string GetSourcesDirectory()
        {
            if (sources.Count > 0)
            {
                var info = new FileInfo(sources.First());
                return info.DirectoryName;
            }
            return null;
        }

        //----------------------------------------
        //Добавление одной страницы pdf (только формат А4. иначе - обрежется)
        bool TryAddPage(PdfDocument document, string path)
        {
            var image = LoadXImage(path);
            if (image != null)
            {
                var page = document.AddPage();
                page.Size = PdfSharp.PageSize.A4;
                if (ImageIsLandscape(image))
                {
                    var w = page.Width;
                    var h = page.Height;
                    page.Height = w;
                    page.Width = h;
                }
                XGraphics gfx = XGraphics.FromPdfPage(page);
                gfx.DrawImage(image, new XPoint(0, 0));
                image.Dispose();
                gfx.Dispose();
                return true;

            }
            return false;

        }

        XImage LoadXImage(string path)
        {
            var imagePath = AssignImage(path);
            if (imagePath != null)
            {
                XImage image = XImage.FromFile(imagePath);
                return image;
            }
            return null;
        }

        bool ImageIsLandscape(XImage img)
        {
            if (img.PointWidth > img.PointHeight)
            {
                return true;
            }
            return false;
        }

        //Если bmp то конвертировать в jpg
        string AssignImage(string path)
        {

            if (!File.Exists(path))
            {
                return null;
            }
            var imageFileInfo = new FileInfo(path);
            if (imageFileInfo.Extension.ToLower() == ".bmp")
            {
                var bitmap = new Bitmap(path);
                string name = imageFileInfo.Name.Split('.')[0];
                return ConvertBMP_JPG(bitmap, name);
            }
            if (imageFileInfo.Extension.ToLower() == ".jpg")
            {
                return path;
            }
            return null;
        }

        string ConvertBMP_JPG(Bitmap bitmap, string name)
        {
            RequireTempFolder();
            EncoderParameters encoderParameters = new EncoderParameters(1);
            encoderParameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 65L);
            bitmap.Save(tempFolder + "\\" + name + ".jpg", GetEncoder(ImageFormat.Jpeg), encoderParameters);
            bitmap.Dispose();
            return tempFolder + "\\" + name + ".jpg";

        }

        void RequireTempFolder()
        {
            if (tempFolder == null)
            {
                tempFolder = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                Directory.CreateDirectory(tempFolder);
            }

        }

        void FlushTempFolder()
        {
            if (tempFolder != null && Directory.Exists(tempFolder))
            {
                System.IO.DirectoryInfo di = new DirectoryInfo(tempFolder);

                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
                foreach (DirectoryInfo dir in di.GetDirectories())
                {
                    dir.Delete(true);
                }

                di.Delete();
            }

        }

        ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();

            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }

            return null;
        }
    }
}
