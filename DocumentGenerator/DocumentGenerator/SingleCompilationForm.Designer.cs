﻿namespace DocumentGenerator
{
    partial class SingleCompilationForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SingleCompilationForm));
            this.PreviewArea = new System.Windows.Forms.GroupBox();
            this.PdfViewer = new AxAcroPDFLib.AxAcroPDF();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.BtnSave = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.DocName = new System.Windows.Forms.TextBox();
            this.DateTo = new System.Windows.Forms.TextBox();
            this.DateFrom = new System.Windows.Forms.TextBox();
            this.IsResult = new System.Windows.Forms.RadioButton();
            this.IsReglament = new System.Windows.Forms.RadioButton();
            this.DateLong = new System.Windows.Forms.CheckBox();
            this.BtnRewrite = new System.Windows.Forms.Button();
            this.BtnRemoveSources = new System.Windows.Forms.Button();
            this.PreviewArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PdfViewer)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // PreviewArea
            // 
            this.PreviewArea.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PreviewArea.Controls.Add(this.PdfViewer);
            this.PreviewArea.Location = new System.Drawing.Point(12, 12);
            this.PreviewArea.Name = "PreviewArea";
            this.PreviewArea.Size = new System.Drawing.Size(899, 358);
            this.PreviewArea.TabIndex = 0;
            this.PreviewArea.TabStop = false;
            this.PreviewArea.Text = "Предпросмотр";
            this.PreviewArea.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // PdfViewer
            // 
            this.PdfViewer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PdfViewer.Enabled = true;
            this.PdfViewer.Location = new System.Drawing.Point(19, 19);
            this.PdfViewer.Name = "PdfViewer";
            this.PdfViewer.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("PdfViewer.OcxState")));
            this.PdfViewer.Size = new System.Drawing.Size(864, 333);
            this.PdfViewer.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.BtnRemoveSources);
            this.groupBox2.Controls.Add(this.BtnRewrite);
            this.groupBox2.Controls.Add(this.BtnSave);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.DocName);
            this.groupBox2.Controls.Add(this.DateTo);
            this.groupBox2.Controls.Add(this.DateFrom);
            this.groupBox2.Controls.Add(this.IsResult);
            this.groupBox2.Controls.Add(this.IsReglament);
            this.groupBox2.Controls.Add(this.DateLong);
            this.groupBox2.Location = new System.Drawing.Point(13, 376);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(898, 60);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Параметры";
            // 
            // BtnSave
            // 
            this.BtnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnSave.Location = new System.Drawing.Point(798, 24);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(26, 20);
            this.BtnSave.TabIndex = 6;
            this.BtnSave.Text = "S";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(445, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(10, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "-";
            // 
            // DocName
            // 
            this.DocName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DocName.Location = new System.Drawing.Point(461, 24);
            this.DocName.Name = "DocName";
            this.DocName.Size = new System.Drawing.Size(331, 20);
            this.DocName.TabIndex = 5;
            this.DocName.Validating += new System.ComponentModel.CancelEventHandler(this.DocName_Validating);
            // 
            // DateTo
            // 
            this.DateTo.Location = new System.Drawing.Point(339, 24);
            this.DateTo.Name = "DateTo";
            this.DateTo.Size = new System.Drawing.Size(100, 20);
            this.DateTo.TabIndex = 4;
            this.DateTo.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.DateTo.Validating += new System.ComponentModel.CancelEventHandler(this.DateTo_Validating);
            // 
            // DateFrom
            // 
            this.DateFrom.Location = new System.Drawing.Point(198, 24);
            this.DateFrom.Name = "DateFrom";
            this.DateFrom.Size = new System.Drawing.Size(100, 20);
            this.DateFrom.TabIndex = 2;
            this.DateFrom.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.DateFrom.Validating += new System.ComponentModel.CancelEventHandler(this.DateFrom_Validating);
            // 
            // IsResult
            // 
            this.IsResult.AutoSize = true;
            this.IsResult.Location = new System.Drawing.Point(93, 24);
            this.IsResult.Name = "IsResult";
            this.IsResult.Size = new System.Drawing.Size(54, 17);
            this.IsResult.TabIndex = 1;
            this.IsResult.TabStop = true;
            this.IsResult.Text = "Отчёт";
            this.IsResult.UseVisualStyleBackColor = true;
            // 
            // IsReglament
            // 
            this.IsReglament.AutoSize = true;
            this.IsReglament.Location = new System.Drawing.Point(7, 24);
            this.IsReglament.Name = "IsReglament";
            this.IsReglament.Size = new System.Drawing.Size(80, 17);
            this.IsReglament.TabIndex = 1;
            this.IsReglament.TabStop = true;
            this.IsReglament.Text = "Регламент";
            this.IsReglament.UseVisualStyleBackColor = true;
            this.IsReglament.Validating += new System.ComponentModel.CancelEventHandler(this.IsReglament_Validating);
            // 
            // DateLong
            // 
            this.DateLong.AutoSize = true;
            this.DateLong.Location = new System.Drawing.Point(304, 26);
            this.DateLong.Name = "DateLong";
            this.DateLong.Size = new System.Drawing.Size(29, 17);
            this.DateLong.TabIndex = 3;
            this.DateLong.Text = "-";
            this.DateLong.UseVisualStyleBackColor = true;
            // 
            // BtnRewrite
            // 
            this.BtnRewrite.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnRewrite.Location = new System.Drawing.Point(830, 24);
            this.BtnRewrite.Name = "BtnRewrite";
            this.BtnRewrite.Size = new System.Drawing.Size(26, 20);
            this.BtnRewrite.TabIndex = 6;
            this.BtnRewrite.Text = "R";
            this.BtnRewrite.UseVisualStyleBackColor = true;
            this.BtnRewrite.Click += new System.EventHandler(this.BtnRewrite_Click);
            // 
            // BtnRemoveSources
            // 
            this.BtnRemoveSources.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnRemoveSources.Location = new System.Drawing.Point(862, 24);
            this.BtnRemoveSources.Name = "BtnRemoveSources";
            this.BtnRemoveSources.Size = new System.Drawing.Size(26, 20);
            this.BtnRemoveSources.TabIndex = 6;
            this.BtnRemoveSources.Text = "D";
            this.BtnRemoveSources.UseVisualStyleBackColor = true;
            this.BtnRemoveSources.Click += new System.EventHandler(this.BtnRemoveSources_Click);
            // 
            // SingleCompilationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(923, 448);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.PreviewArea);
            this.Name = "SingleCompilationForm";
            this.Text = "Генерация документа";
            this.PreviewArea.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PdfViewer)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox PreviewArea;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox DateFrom;
        private System.Windows.Forms.RadioButton IsResult;
        private System.Windows.Forms.RadioButton IsReglament;
        private System.Windows.Forms.CheckBox DateLong;
        private System.Windows.Forms.TextBox DateTo;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox DocName;
        private AxAcroPDFLib.AxAcroPDF PdfViewer;
        private System.Windows.Forms.Button BtnRemoveSources;
        private System.Windows.Forms.Button BtnRewrite;
    }
}

