﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DocumentGenerator
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            var lf = new LoadingForm();
            lf.Show();
            Thread.Sleep(200);
            var compiler = CompilerSwitch.AssignCompilerTo(args);
            lf.Close();
            if (compiler != null)
            {
                Application.Run(new SingleCompilationForm(compiler));
            }else
            {
                MessageBox.Show("Исходники обработаны в автоматическом режиме");
            }
            
        }
    }
}
