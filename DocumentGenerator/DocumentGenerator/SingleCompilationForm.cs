﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DocumentGenerator
{
    public partial class SingleCompilationForm : Form
    {
        IPdfCompiler compiler;
        string folder = null;
        string filename = null;
        string subfolder = null;
        string subfolder_path =null;

        public SingleCompilationForm(IPdfCompiler compiler)
        {
            InitializeComponent();
            this.compiler = compiler;
            var preview = compiler.PreviewDocument();
            if (preview != null)
            {
                PdfViewer.LoadFile(preview);
            }
        }

      

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        //Сохранить
        private void BtnSave_Click(object sender, EventArgs e)
        {
           

            if (!IsReglament.Checked && !IsResult.Checked)
            {           
                return;
            }

            //Проверка на дату
            var dateRgfx = new Regex(@"\d\d\.\d\d.\d\d");
            if (!dateRgfx.IsMatch(DateFrom.Text) && DateLong.Checked)
            {
                return;
            }
            if (!dateRgfx.IsMatch(DateTo.Text) && DateLong.Checked)
            {
                return;
            }

            if (DocName.Text.Length == 0)
            {
                return;
            }

            //================Создание папки если не существует и сохранение файлов==============
         
            string root = Directory.GetCurrentDirectory().Replace("\\","/");

            if (IsReglament.Checked)
            {
                folder = "! Регламенты мероприятий";
                filename = "Регламент.pdf";
            }
            if (IsResult.Checked)
            {
                folder = "! Отчёты мероприятий";
                filename = "Отчёт.pdf";
            }

            subfolder = DateFrom.Text + (DateLong.Checked ? ("-" + DateTo.Text) : "") + " - " + DocName.Text;
            subfolder_path = root + "/" + folder + "/" + subfolder;
            //
            if (!Directory.Exists(root+"/"+folder+"/"+subfolder))
            {
                Directory.CreateDirectory(root + "/" + folder + "/" + subfolder);
            }
            //
            if (compiler.CopyTo(root + "/" + folder + "/" + subfolder+"/"+filename))
            {
                MessageBox.Show("Файл записан в директорию "+subfolder_path);
                BtnSave.Enabled = false;
                BtnRewrite.Enabled = false;
            }
           
        }

        private void DateFrom_Validating(object sender, CancelEventArgs e)
        {
           
        }

        private void DateTo_Validating(object sender, CancelEventArgs e)
        {
            
        }

        private void DocName_Validating(object sender, CancelEventArgs e)
        {
           
        }

        private void IsReglament_Validating(object sender, CancelEventArgs e)
        {
           
        }

        //Изменить название исходного pdf
        private void BtnRewrite_Click(object sender, EventArgs e)
        {
            if (!IsReglament.Checked && !IsResult.Checked)
            {
                return;
            }
            if (IsReglament.Checked)
            {
                filename = "Регламент.pdf";
            }
            if (IsResult.Checked)
            {
                filename = "Отчёт.pdf";
            }
            subfolder_path = compiler.GetSourcesDirectory();
            if (compiler.CopyTo(subfolder_path + "/" + filename))
            {
                MessageBox.Show("Файл записан в директорию исходников");
                BtnSave.Enabled = false;
                BtnRewrite.Enabled = false;
            };
            
        }

        //Удалить исходные файлы
        private void BtnRemoveSources_Click(object sender, EventArgs e)
        {
            if (!IsReglament.Checked && !IsResult.Checked)
            {
                return;
            }
            if (IsReglament.Checked)
            {
                filename = "Регламент.pdf";
            }
            if (IsResult.Checked)
            {
                filename = "Отчёт.pdf";
            }
            if (File.Exists(subfolder_path + "/" + filename))
            {
                if (compiler.RemoveSources())
                {
                    MessageBox.Show("Все исходники удалены");
                    BtnRemoveSources.Enabled = false;
                }
                else
                {
                    MessageBox.Show("Не удалось удалить все исходники");
                }
            }
        }
    }
}
